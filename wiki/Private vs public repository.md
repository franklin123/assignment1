
If your application is closed-source, it should go in a private repository. Copyright might protect you legally, but if someone decides to steal your code, they likely won't care. Then it's up to you to be able to find these people, be able to identify that they've used your source code in their application, and prove it to a judge.

This is especially problematic if you're doing something new. Maybe you've created a new algorithm of sorts, or combined web-services or APIs in a way not yet done. You're releasing your application for money, and leaving a comprehensive how-to guide online for all to see, significantly damaging your competitive advantage.

If you're handling user security in any way, any mistakes you made (and you will have) will be much more easily found and exploited if attackers can read your source code. Some online repositories have bug trackers -- people can follow that, and it will point them straight to the vulnerabilities that have been found, and will have a window to take advantage of them (the time it takes you to fix the bugs, and for your users to apply a patch).

I should also mention that if you have a team of five people or less, you can host your code privately with BitBucket for free. There are other services I'm sure, I just know of that one because I use it.