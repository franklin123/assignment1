## Getting started
Assignment1 is structured as a placeholder for assignments. The Owner is expected to always update it with most recent information. 
## Clone a repository
Clone the repository from bitbucket

````` bash
git clone https://bitbucket.org/franklin123/assignment1/src/master/

For detailed explanation on how things work, check out [bitbucket](https://bitbucket.org/product/guides).

## MIT License

This repository and the code inside it is licensed under the MIT License. Read LICENSE for more information.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.